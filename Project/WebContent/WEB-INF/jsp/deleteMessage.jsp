
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>userdelete</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>


    <p><a  href="Logout" class="text-danger">ログアウト</a></p>



    <h3>
        <div class="mx-auto" style="width: 200px;">
            <br>  ${artistList.artistName}</br>
        </div>
    </h3>

<form class="form-signin" action="DeleteMessage" method="post">

    <br>


    <div class="form-group row">
        <label for="staticUsername" class="col-sm-2 col-form-label"><strong>ユーザ名</strong></label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticUsername" value=" ${messageList.userName}">
        </div>
    </div>

    <div class="form-group row">
        <label for="staticBirthday" class="col-sm-2 col-form-label"><strong>メッセージ</strong></label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticBirthday" value=" ${messageList.message}">
        </div>
    </div>

    <div class="form-group row">
        <label for="staticRegisterday" class="col-sm-2 col-form-label"><strong>投稿時刻</strong></label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticRegisterday"
                value=" ${messageList.createDate}">
        </div>
    </div>

    <h5>
        <div class="mx-auto" style="width: 200px;">
            <br>削除しますか？</br>
        </div>
    </h5>
</br>
<input type="hidden" name="id" value="${messageList.id}">
<input type="hidden" name="artistId" value="${artistList.id}">

    <br></br>


    <div class="mx-auto" style="width: 200px;">



        <a  href="MessageList?id=${artistList.id }" class="btn btn-primary btn-lg">戻る</a>
        <button type="submit" class="btn btn-secondary btn-lg">削除</button>



    </div>
</form>

</body>

</html>