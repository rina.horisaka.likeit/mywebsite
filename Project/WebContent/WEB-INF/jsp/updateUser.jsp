
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>userupdate</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>

    <p><a href="ArtistList" class="text-primary">戻る</a></p>
    <p><a href="Logout" class="text-danger">ログアウト</a></p>




    <h3>
        <div class="mx-auto" style="width: 200px;">
            マイページ
        </div>
    </h3>


<c:if test="${errMsg != null}">
		<p class="text-danger">${errMsg}</p>


	</c:if>


   <form class="form-signin" action="UpdateUser" method="post">

        <div class="form-group row">
            <label for="staticLoginid" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">
                <input type="text"name="loginId" readonly class="form-control-plaintext" id="staticLoginid" value="${userInfo.loginId}">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
            <div class="col-sm-10">
                <input type="password"name="password" class="form-control" id="inputPassword"  placeholder="●●●●●">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputUsername" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-10">
                <input type="text"  name="userName" class="form-control" id="inputUsername"value="${userInfo.userName}" >
            </div>
        </div>


    <div class="mx-auto" style="width: 200px;">
        <button type="submit" class="btn btn-primary">更新</button>
    </div>
 </form>



    <h1></h1>










</body>

</html>