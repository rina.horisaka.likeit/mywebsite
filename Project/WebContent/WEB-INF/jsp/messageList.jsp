
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>alluser</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <p><a href="ArtistList" class="text-primary">戻る</a></p>
    <p><a href="Logout" class="text-danger">ログアウト</a></p>




    <h2>
        <div class="mx-auto" style="width: 200px;">
            ${artistList.artistName}
        </div>
    </h2>



<c:if test="${errMsg != null}">
		<p class="text-danger">${errMsg}</p>


	</c:if>




     <form class="form-signin" action="MessageList" method="post">


        <div class="form-group row">
            <label for="staticUsername" class="col-sm-2 col-form-label"><strong>ユーザ名</strong></label>
            <div class="col-sm-10">
                <input type="text" name="userName" readonly class="form-control-plaintext" id="staticUsername" value="${userInfo.userName}">
            </div>
        </div>
          <div class="form-group">
            <label for="exampleFormControlTextarea1">メッセージ</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="message"  placeholder="message"></textarea>
          </div>

<input type="hidden" name="artistId" value=" ${artistList.id}">

    <div class="mx-auto" style="width: 200px;">
        <button type="submit" class="btn btn-primary">投稿</button>
    </div>
 </form>


    <h1></h1>


			<c:forEach var="message" items="${messageList}">
			<br><br><br><br>
    <div class="form-group row">
        <label for="staticUsername" class="col-sm-2 col-form-label"><strong>ユーザ名</strong></label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticUsername" value="${message.userName}">
        </div>
    </div>

    <div class="form-group row">
        <label for="staticBirthday" class="col-sm-2 col-form-label"><strong>メッセージ</strong></label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticBirthday" value="${message.message}">
        </div>
    </div>

    <div class="form-group row">
        <label for="staticRegisterday" class="col-sm-2 col-form-label"><strong>投稿時刻</strong></label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticRegisterday"
                value="${message.createDate}">
        </div>
    </div>

<c:if test="${ message.loginId == userInfo.loginId}">

  <a  type="submit" href="DeleteMessage?id=${message.id}&artistId=${artistList.id}" class="btn btn-danger" >削除</a>


							</c:if>

</c:forEach>

</body>

</html>