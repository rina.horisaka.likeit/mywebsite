package beans;

import java.io.Serializable;

public class UserBeans implements Serializable {
	private int id;
	private String loginId;
	private String userName;
	private String password;

	// ログインセッションを保存するためのコンストラクタ
		public UserBeans(String loginId, String userName) {
			this.loginId = loginId;
			this.userName = userName;
		}


	// 全てのデータをセットするコンストラクタ
	public UserBeans(int id, String loginId, String userName, String password){
		this.id = id;
		this.loginId = loginId;
		this.userName = userName;
		this.password = password;
	}



	public UserBeans() {

	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}




}

