package beans;

import java.io.Serializable;

public class MessageBeans implements Serializable {
	private int id;
	private String message;
	private String createDate;
	private int userId;
	private int artistId;
	private String userName;
	private String loginId;

	// 全てのデータをセットするコンストラクタ
	public MessageBeans(int id, String message,  String createDate,
			int userId, int artistId) {
		this.id = id;
		this.message = message;
		this.createDate = createDate;
		this.userId = userId;
		this.artistId = artistId;
	}




	public MessageBeans(int id, String loginId, String message, String createDate, String userName) {
		this.id = id;
		this.setLoginId(loginId);
		this.message = message;
		this.createDate = createDate;
		this.setUserName(userName);
	}


	public MessageBeans(int id, String message, String createDate, String userName) {
		this.id = id;
		this.message = message;
		this.createDate = createDate;
		this.userName = userName;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getArtistId() {
		return artistId;
	}
	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getLoginId() {
		return loginId;
	}




	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}


}
