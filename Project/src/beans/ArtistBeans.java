package beans;

import java.io.Serializable;

public class ArtistBeans implements Serializable {
	private int id;
	private String artistName;



	// 全てのデータをセットするコンストラクタ
	public ArtistBeans(int id, String artistName) {
		this.id = id;
		this.artistName = artistName;
	}


	public ArtistBeans() {

	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}



}

