package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UpdateUser
 */
@WebServlet("/UpdateUser")
public class UpdateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("LoginUser");
			return;
		}


		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/updateUser.jsp");
		dispatcher.forward(request, response);

	}




	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの入力項目を取得

				String loginId = request.getParameter("loginId");
				String password = request.getParameter("password");
				String userName = request.getParameter("userName");

				/** 更新に失敗した場合 **/

				// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				UserDao userDao = new UserDao();

				//未入力の項目がある場合

				if ( userName.equals("") || password.equals("")) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "入力された内容は正しくありません");



					// 入力画面にフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/updateUser.jsp");
					dispatcher.forward(request, response);
					return;

				}

				/** 更新に成功した場合 **/
				 else {
					 String i=Password.pass(password);

					// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
					userDao.update(loginId, i, userName );
				}
				// アーティスト一覧サーブレットにリダイレクト
				response.sendRedirect("ArtistList");

			}


	}


