package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ArtistBeans;
import beans.MessageBeans;
import beans.UserBeans;
import dao.ArtistDao;
import dao.MessageDao;

/**
 * Servlet implementation class DeleteMessage
 */
@WebServlet("/DeleteMessage")
public class DeleteMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteMessage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("LoginUser");
			return;
		}


		String artistId = request.getParameter("artistId");

		// アーティスト情報を取得
		ArtistDao artistDao = new ArtistDao();
		ArtistBeans artistList = artistDao.findArtistName(artistId);

		// リクエストスコープにアーティスト情報をセット
		request.setAttribute("artistList", artistList);



		String Id = request.getParameter("id");

		// メッセージ一覧情報を取得
		MessageDao messageDao = new MessageDao();
		MessageBeans messageList = messageDao.findMessageDetails(Id);

		// リクエストスコープにメッセージ一覧情報をセット
		request.setAttribute("messageList", messageList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/deleteMessage.jsp");
		dispatcher.forward(request, response);
	}





	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの入力項目を取得
						String id = request.getParameter("id");
						String artistId = request.getParameter("artistId");

				MessageDao messageDao = new MessageDao();
				messageDao.delete(id);





//				// リクエストスコープにアーティスト情報をセット
//				request.setAttribute("artistid", artistId);
//				System.out.println("Id="+artistId);

				// セッションにユーザの情報をセット
				HttpSession session = request.getSession();
				session.setAttribute("artistId", artistId);

				// メッセージ一覧サーブレットにリダイレクト
						response.sendRedirect("MessageList");



	}
}
