package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ArtistBeans;
import beans.MessageBeans;
import beans.UserBeans;
import dao.ArtistDao;
import dao.MessageDao;
import dao.UserDao;

/**
 * Servlet implementation class MessageList
 */
@WebServlet("/MessageList")
public class MessageList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageList() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
				HttpSession session = request.getSession();
				UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
				if (userInfo == null) {
					response.sendRedirect("LoginUser");
					return;
				}

				String Id = request.getParameter("id");
				if(Id==null) {
					Id =  (String) session.getAttribute("artistId");
					System.out.println("Id:"+Id);

				}

				// アーティスト情報を取得
				ArtistDao artistDao = new ArtistDao();
				ArtistBeans artistList = artistDao.findArtistName(Id);

				// リクエストスコープにアーティスト情報をセット
				request.setAttribute("artistList", artistList);



				// メッセージ一覧情報を取得
				MessageDao messageDao = new MessageDao();
				List<MessageBeans> messageList = messageDao.findAll(Id);

				// リクエストスコープにメッセージ一覧情報をセット
				request.setAttribute("messageList", messageList);



				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/messageList.jsp");
				dispatcher.forward(request, response);

			}





	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの入力項目を取得
				String message = request.getParameter("message");
				String artistId = request.getParameter("artistId");

				HttpSession session = request.getSession();
				UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

				//ユーザIDを取得する
				UserDao userDao =  new UserDao();
				int userId = userDao.findUserId(userInfo.getLoginId());



				// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				MessageDao messageDao = new MessageDao();

				// アーティスト情報を取得
				String Id = request.getParameter("artistId");

				ArtistDao artistDao = new ArtistDao();
				ArtistBeans artistList = artistDao.findArtistName(Id);

				// リクエストスコープにアーティスト情報をセット
				request.setAttribute("artistList", artistList);



				/** 登録に失敗した場合（メッセージが空白の場合） **/
				if ( message.equals("") ) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "メッセージを入力して下さい");


					// メッセージ一覧情報を取得
					List<MessageBeans> messageList = messageDao.findAll(Id);

					// リクエストスコープにメッセージ一覧情報をセット
					request.setAttribute("messageList", messageList);



					// 入力画面にフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/messageList.jsp");
					dispatcher.forward(request, response);
					return;

				}


				/** 登録に成功した場合 **/
				 else {


						// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
					 messageDao.register(message,userId,artistId);
					}



				// メッセージ一覧情報を取得
				List<MessageBeans> messageList = messageDao.findAll(Id);

				// リクエストスコープにメッセージ一覧情報をセット
				request.setAttribute("messageList", messageList);



				// 入力画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/messageList.jsp");
				dispatcher.forward(request, response);
				return;
			}




	}


