package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.MessageBeans;

public class MessageDao {

	//メッセージを登録する
		public void register(String message,int userId,String artistId) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = " INSERT INTO message (message,create_date,user_id,artist_id) value(?,now(),?,?)";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, message);
				pStmt.setInt(2, userId);
				pStmt.setString(3, artistId);


				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}


		//全てのメッセージを取得する

		public List<MessageBeans> findAll(String Id) {
			Connection conn = null;
			List<MessageBeans> messageList = new ArrayList<MessageBeans>();

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT m.id,u.login_id,m.message,m.create_date,u.user_name FROM message m inner join user u  on m.user_id = u.id where artist_id = '" + Id + "' order by create_date desc";


				// SELECTを実行し、結果表を取得
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				// 結果表に格納されたレコードの内容を
				// Userインスタンスに設定し、ArrayListインスタンスに追加
				while (rs.next()) {
					int id = rs.getInt("id");
					String loginId = rs.getString("login_id");
					String message = rs.getString("message");
					String createDate = rs.getString("create_date");
					String userName = rs.getString("user_name");
					MessageBeans allMessage = new MessageBeans(id,loginId, message, createDate, userName);

					messageList.add(allMessage);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return messageList;
		}

		//メッセージの情報を参照する

		public MessageBeans findMessageDetails(String Id) {
			Connection conn = null;

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				// メッセージの情報を参照する
				String sql = "SELECT m.id,m.message,m.create_date,u.user_name FROM message m inner join user u on m.user_id = u.id WHERE m.id=" + Id;

				// SELECTを実行し、結果表を取得
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				// 結果表に格納されたレコードの内容を
				// messageDetailsに設定し、MessageListインスタンスに追加
				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;
				}

				int id = rs.getInt("id");
				String message = rs.getString("message");
				String createDate = rs.getString("create_date");
				String userName = rs.getString("user_name");
				MessageBeans messageDetails = new MessageBeans(id, message,createDate, userName);

				return messageDetails;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//メッセージの情報を削除する

		public void delete(String id) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = " delete from message WHERE id ='" + id + "'";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}

}
