package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.ArtistBeans;

public class ArtistDao {


	//アーティストの一覧を取得

	public List<ArtistBeans> findArtist() {
		Connection conn = null;
		List<ArtistBeans> artistList = new ArrayList<ArtistBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// アーティストを取得するSQL文
			String sql = "SELECT * FROM artist";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// artistListインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String artistName = rs.getString("artist_name");
				ArtistBeans artist = new ArtistBeans(id,artistName);

				artistList.add(artist);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return artistList;
	}

	//アーティストの名前を探す

		public ArtistBeans findArtistName(String Id) {
			Connection conn = null;

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				// アーティストの情報を参照する
				String sql = "SELECT * FROM artist WHERE id=" + Id;

				// SELECTを実行し、結果表を取得
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				// 結果表に格納されたレコードの内容を
				// Artistインスタンスに設定し、ArrayListインスタンスに追加
				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;
				}

				int id = rs.getInt("id");
				String artistName = rs.getString("artist_name");
				ArtistBeans user = new ArtistBeans(id,artistName);

				return user;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

}