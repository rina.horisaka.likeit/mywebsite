package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.UserBeans;

public class UserDao {

	//ログイン情報を調べる

	public UserBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String userLoginId = rs.getString("login_id");
			String name = rs.getString("user_name");
			return new UserBeans(userLoginId, name);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ログインIDを調べる

		public String findByLoginId(String loginId) {

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM user WHERE login_id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				ResultSet rs = pStmt.executeQuery();

				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;
				}

				// 必要なデータのみインスタンスのフィールドに追加
				String loginID = rs.getString("login_id");
				return loginID;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}


		// 新規ユーザーを登録する

		public void register(String loginId, String password, String userName) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = " INSERT INTO user (login_id,user_name,password) VALUES (?,?,?)";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, userName);
				pStmt.setString(3, password);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}



		//ユーザの情報を更新する

		public void update(String loginId, String password, String userName) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = " UPDATE user SET  user_name=?, password =? WHERE login_id ='" + loginId + "'";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, userName);
				pStmt.setString(2, password);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}


		//ユーザIDを調べる
		public int findUserId(String loginId) {

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT id FROM user WHERE login_id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				ResultSet rs = pStmt.executeQuery();

				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return (Integer) null;
				}

				// 必要なデータのみインスタンスのフィールドに追加
				int ID = rs.getInt("id");
				return ID;

			} catch (SQLException e) {
				e.printStackTrace();
				return (Integer) null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return (Integer) null;
					}
				}
			}
		}


}